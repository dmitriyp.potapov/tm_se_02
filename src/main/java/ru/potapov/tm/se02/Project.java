package ru.potapov.tm.se02;

import java.util.ArrayList;
import java.util.List;

public class Project {
    private String      name;
    private List<Task>  listTasks;

    public Project(String name) {
        this.name       = name;
        this.listTasks  = new ArrayList<>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Task> getListTasks() {
        return listTasks;
    }

    public void setListTasks(ArrayList<Task> listTasks) {
        this.listTasks = listTasks;
    }

    public void clearTasks(){
        listTasks.clear();
    }

    public void remove(Task task){
        listTasks.remove(task);
    }

    @Override
    public String toString() {
        return "Project [" + getName() + "]";
    }
}
