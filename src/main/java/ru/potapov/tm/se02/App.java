package ru.potapov.tm.se02;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Second app **
 */
public class App {
    private boolean         exit;
    private List<Project>   listProjects;
    private Project         currentProject;

    public App() {
        this.exit = false;
        listProjects = new ArrayList<>();
        currentProject = null;

    }

    public static void main(String[] args) {
        new App().go();
    }

    public void go() {
        Scanner in = new Scanner(System.in);

        while (!exit) {
            System.out.println("\nInsert your command in low case:");
            String strIn = in.nextLine();
            String[] arrCommand = strIn.split(" ");

            chooseCommand(arrCommand);
        }
    }

    private void chooseCommand(String... arrCommand) {
        String cmd = arrCommand[0];

        if ("help".equals(cmd)) {
            commandHelp();

        } else if ("exit".equals(cmd)) {
            commandExit();

        } else if ("create-project".equals(cmd)) {
            commandCreateProject(arrCommand);

        } else if ("read-projects".equals(cmd)) {
            commandReadProjects();

        } else if ("delete-all-projects".equals(cmd)) {
            commandDeleteAllProjects();

        } else if ("delete-project".equals(cmd)) {
            commandDeleteProject(arrCommand);

        } else if ("choose-project".equals(cmd)) {
            commandChooseProject(arrCommand);

        } else if ("update-project".equals(cmd)) {
            commandUpdateProject(arrCommand);

        } else if ("create-task".equals(cmd)) {
            commandCreateTask(arrCommand);

        } else if ("read-tasks".equals(cmd)) {
            commandReadTasks();

        } else if ("read-all-tasks".equals(cmd)) {
            commandReadAllTasks();

        } else if ("delete-tasks".equals(cmd)) {
            commandDeleteTasks();

        } else if ("delete-all-tasks".equals(cmd)) {
            commandDeleteAllTasks();

        } else if ("update-task".equals(cmd)) {
            commandUpdateTask(arrCommand);

        } else if ("delete-task".equals(cmd)) {
            commandDeleteTask(arrCommand);

        } else {
            printNotCorrectCommand();

        }
    }

    private void commandDeleteTask(String[] arrCommand) {
        if (arrCommand.length < 2) {
            printNotCorrectCommand();
            return;
        }

        Task findTask = null;
        if (Objects.isNull(currentProject)) {
            printNotChoosenCurrentProject();
            return;
        }

        for (Task task : currentProject.getListTasks()) {
            if (task.getName().equals(arrCommand[1])) {
                findTask = task;
            }
        }

        if (Objects.isNull(findTask)) {
            System.out.println("There isn't any task with name: " + arrCommand[1] + " in the project " + currentProject);
            return;
        }

        currentProject.remove(findTask);
        System.out.println("The task has deleted");
    }

    private void commandUpdateTask(String[] arrCommand) {
        if (arrCommand.length < 3) {
            printNotCorrectCommand();
            return;
        }
        if (Objects.isNull(currentProject)) {
            printNotChoosenCurrentProject();
            return;
        }

        Task findTask = null;
        for (Task task : currentProject.getListTasks()) {
            if (task.getName().equals(arrCommand[1])) {
                findTask = task;
            }
        }

        if (Objects.isNull(findTask)) {
            System.out.println("There isn't any task with name: " + arrCommand[1] + " in the project " + currentProject);
            return;
        }

        findTask.setName(arrCommand[2]);
        System.out.println("The task has updated");
    }

    private void commandDeleteAllTasks() {
        for (Project project : listProjects) {
            project.clearTasks();
        }
        System.out.println("All tasks of all projects have deleted");
    }

    private void commandDeleteTasks() {
        if (Objects.isNull(currentProject)) {
            printNotChoosenCurrentProject();
            return;
        }

        currentProject.clearTasks();
    }

    private void commandReadAllTasks() {
        if (listProjects.size() == 0) {
            System.out.println("We have not any project");
            return;
        }
        for (Project project : listProjects) {
            System.out.println("Project [" + project.getName() + "]:");
            System.out.println(project.getListTasks());
        }
    }

    private void commandReadTasks() {
        if (listProjects.size() == 0)
            System.out.println("We have not any project");
        else if (Objects.nonNull(currentProject))
            System.out.println(currentProject.getListTasks());
        else
            printNotCorrectCommand();
    }

    private void commandCreateTask(String[] arrCommand) {
        if (arrCommand.length < 2) {
            printNotCorrectCommand();
            return;
        }

        if (Objects.isNull(currentProject)) {
            printNotChoosenCurrentProject();
            return;
        }

        boolean exist = false;
        for (Task task : currentProject.getListTasks()) {
            if (task.getName().equals(arrCommand[1])) {
                exist = true;
            }
        }
        if (exist) {
            System.out.println("Task with name [" + arrCommand[1] + "] already exist");
            return;
        }

        Task newTask = new Task(arrCommand[1]);
        currentProject.getListTasks().add(newTask);
        System.out.println("Task [" + newTask.getName() + "] has created in the project [" + currentProject.getName() + "]");
    }

    private void commandUpdateProject(String[] arrCommand) {
        if (arrCommand.length < 3) {
            printNotCorrectCommand();
            return;
        }

        Project findProject = null;
        for (Project project : listProjects) {
            if (project.getName().equals(arrCommand[1])) {
                findProject = project;
            }
        }

        if (Objects.isNull(findProject)) {
            System.out.println("There isn't any project with name: " + arrCommand[1]);
            currentProject = null;
            return;
        }

        currentProject = findProject;
        findProject.setName(arrCommand[2]);
        System.out.println("The project has updated");
    }

    private void commandChooseProject(String[] arrCommand) {
        if (arrCommand.length < 2) {
            printNotCorrectCommand();
            return;
        }

        Project findProject = null;
        for (Project project : listProjects) {
            if (project.getName().equals(arrCommand[1])) {
                findProject = project;
            }
        }

        if (Objects.isNull(findProject)) {
            System.out.println("There isn't any project with name: " + arrCommand[1]);
            currentProject = null;
            return;
        }

        currentProject = findProject;
        System.out.println("Project [" + currentProject.getName() + "] has chosen");

    }

    private void commandDeleteProject(String[] arrCommand) {
        if (arrCommand.length < 2) {
            printNotCorrectCommand();
            return;
        }

        Project findProject = null;
        for (Project project : listProjects) {
            if (project.getName().equals(arrCommand[1])) {
                findProject = project;
            }
        }

        if (Objects.isNull(findProject)) {
            System.out.println("There isn't any project with name: " + arrCommand[1]);
            return;
        }

        listProjects.remove(findProject);
        System.out.println("Project [" + currentProject.getName() + "] has deleted");
    }

    private void commandDeleteAllProjects() {
        listProjects.clear();
        System.out.println("All progects have deleted \n");
    }

    private void commandReadProjects() {
        System.out.println("All projects: \n");
        if (listProjects.size() == 0)
            System.out.println("We have not any project");
        else
            System.out.println(listProjects.toString());
    }

    private void commandCreateProject(String[] arrCommand) {
        if (arrCommand.length < 2) {
            printNotCorrectCommand();
            return;
        }

        boolean exist = false;
        for (Project project : listProjects) {
            if (project.getName().equals(arrCommand[1])) {
                exist = true;
            }
        }
        if (exist) {
            System.out.println("Project with name [" + arrCommand[1] + "] already exist");
            return;
        }

        currentProject = new Project(arrCommand[1]);
        listProjects.add(currentProject);
        System.out.println("Project [" + currentProject.getName() + "] has created");
    }

    private void commandHelp() {
        String help = "There are general commands: \n" +
                "   help:                               lists all command\n" +
                "   exit:                               halts the programm\n" +

                "   create-project <Name>:              creates new project\n" +
                "   read-projects:                      reads all projects\n" +
                "   choose-project <Name>:              chooses Nth the project\n" +
                "   update-project <Name> <New Name>:   update name of the Nth project\n" +
                "   delete-project <Name>:              deletes the project with name <Name>\n" +
                "   delete-all-projects:                 deletes all project\n" +

                "   create-task:                        creates new task\n" +
                "   read-tasks:                         reads tasks for a current project\n" +
                "   read-all-tasks:                     reads all tasks\n" +
                "   update-task <Name> <New Name>:      update name of th Nth task\n" +
                "   delete-task <Name>:                 deletes the task with name <Name> of the cuurrent project\n" +
                "   delete-tasks:                       deletes all tasks of a current project\n" +
                "   delete-all-tasks:                   deletes all tasks of all projects\n";
        System.out.println(help);
    }

    private void commandExit() {
        System.out.println("Buy-buy....");
        exit = true;
    }


    private void printNotCorrectCommand() {
        System.out.println("not correct command, plz try again or type command <help> \n");
    }

    private void printNotChoosenCurrentProject() {
        System.out.println("There isn't any choosen projects! Plz, choose a project!");
    }
}
